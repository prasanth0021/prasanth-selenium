package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week5.day1.Learnreport;

public class SeMethods extends Learnreport implements WdMethods{
	public int i = 1;
	public static  RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions obj = new ChromeOptions();
				obj.addArguments("--disable-notifications");
				driver = new ChromeDriver(obj);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportstep("The Browser "+browser+" Launched Successfully", "pass");
		} catch (WebDriverException e) {
			reportstep("The Browser "+browser+" not Launched" , "fail");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator.toLowerCase()) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "linktext" : return driver.findElementByLinkText(locValue);


			}
			reportstep("The Element found", "pass");
		} catch (NoSuchElementException e) {
			reportstep("The Element is not found", "fail");
		} catch (Exception e) {
			reportstep("Unknow Exception ", "fail");
		}
		return null;
	}



	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementsById(locValue);
			case "class" : return driver.findElementsByClassName(locValue);
			case "xpath" : return driver.findElementsByXPath(locValue);
			case "linktext" : return driver.findElementsByLinkText(locValue);


			}
			reportstep("The Element found", "pass");
		} catch (NoSuchElementException e) {
			reportstep("The Element is not found", "fail");
		} catch (Exception e) {
			reportstep("Unknow Exception ", "fail");
		}
		return null;
	}
	
	
	

	public String getsize(List<WebElement> ele)
	{
		try {
			 int size = ele.size();	
			System.out.println(size);
			reportstep(" size is printed succussfully ", "pass");
		} catch (WebDriverException e) {
			reportstep("size not entered ", "fail");
		}finally {
			takeSnap();
		}
		return null;
	}

	
	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportstep("The data "+data+" is Entered Successfully", "pass");
		} catch (WebDriverException e) {
			reportstep("The data "+data+" is Not Entered", "fail");
		} finally {
			takeSnap();
		}
	}

	
	
	

	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			reportstep("The Element "+ele+" Clicked Successfully", "pass");
		} catch (Exception e) {
			reportstep("The Element "+ele+"is not Clicked", "fail");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportstep("The Element "+ele+" Clicked Successfully", "pass");
		} catch (Exception e) {
			reportstep("The Element "+ele+"is not Clicked", "fail");
		}
		finally {
			takeSnap();
		}
	}

	
	@Override
	public String getText(WebElement ele) {
		String text = null;
		try {
			 text = ele.getText();
			System.out.println(text);
			reportstep(" text is printed succussfully ", "pass");
		} catch (WebDriverException e) {
			reportstep("ther is no error message", "fail");
		}finally {
			takeSnap();
		}
		return text;
	}

	public String getText(List<WebElement> ele)
	{
	
		
		try {
			for (WebElement text : ele) {
				System.out.println(text.getText());
			}
		} catch (WebDriverException e) {
			System.out.println(" handle the error");
		}finally {
			takeSnap();
		}
		return null;
		
	}
	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportstep("The DropDown Is Selected with VisibleText ", "pass");
		} catch (Exception e) {
			reportstep("The DropDown Is not Selected with VisibleText "+value, "fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select ddi = new Select(ele);
			ddi.selectByIndex(index);
			reportstep("The DropDown Is Selected with VisibleText ", "pass");
		} catch (Exception e) {
			reportstep("The DropDown Is not Selected with Visible "+index, "fail");
		} finally {
			takeSnap();
		}


	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String title = driver.getTitle();
		if (title.equalsIgnoreCase(expectedTitle)) {
			System.out.println("tittle matched  "+title);
			reportstep("tittle matched", "pass");

		}
		else
		{
			System.out.println("tittle un matched "+title);
			reportstep("tittle un matched", "fail");
		}
		return false;
	}
	
	
	public boolean verifyPartialTitle(String expectedTitle) {
		String title = driver.getTitle();
		if (title.contains(expectedTitle)) {
			System.out.println("tittle matched  "+title);
			reportstep("tittle matched", "pass");

		}
		else
		{
			System.out.println("tittle un matched "+title);
			reportstep("tittle un matched", "fail");
		}
		return false;
	}
	
  	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if (text.equals(expectedText)) {
			System.out.println("text matched");
			reportstep("text matched", "pass");

		}
		else
		{
			System.out.println("text un matched");
			reportstep("text un matched", "fail");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if (text.equals(expectedText)) {
			System.out.println("text matched");
			reportstep("text matched", "pass");

		}
		else
		{
			System.out.println("text un matched");
			reportstep("text un matched", "fail");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		boolean b = ele.isSelected();
		if(b)
		{
			System.out.println("selected succusssfully");
			reportstep("selected succussfully", "pass");
		}
		else
		{
			System.out.println("not selected");
			reportstep("not selected", "fail");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		boolean a = ele.isDisplayed();
		if(a)
		{
			System.out.println("displayed succusssfully");
			reportstep("displayed succussfully", "pass");
		}
		else
		{
			System.out.println("not displayed");
			reportstep("not displayed", "fail");
		}


	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
			Set<String> size = driver.getWindowHandles();
			System.out.println("Number of windows appearing" +size);
			List<String> getwindow = new ArrayList<String>();
			getwindow.addAll(size);
			String window = getwindow.get(index);
			driver.switchTo().window(window);
			driver.manage().window().maximize();
		} catch (WebDriverException e) {
			reportstep("Cannot switch to another window", "fail");

		}finally {
			takeSnap();
		}

	}


	@Override
	public void switchToParentWindow()
	{
		try {
			Set<String> allwindows = driver.getWindowHandles();
			for (String windows : allwindows) {
				driver.switchTo().window(windows);
			}
			driver.manage().window().maximize();
			System.out.println(driver.getCurrentUrl());
		} catch (WebDriverException e) {
			System.out.println(" cannot switch to window ");
		}finally {
			takeSnap();
		}
	}


	@Override
	public void switchToLastWindow()
	{
		try {
			Set<String> lastwindows = driver.getWindowHandles();
			for (String windows : lastwindows) {
				driver.switchTo().window(windows);
			}
			driver.manage().window().maximize();
			System.out.println(driver.getCurrentUrl());
		} catch (WebDriverException e) {
			System.out.println(" cannot switch to window ");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			driver.switchTo().defaultContent();
			reportstep("frame switched succussfully", "pass");
		} catch (WebDriverException e) {
			reportstep("frame swiched un succussfully", "fail");
		}finally {
			takeSnap();

		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (WebDriverException e) {
			reportstep(" alert unhandled ", "fail");
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
		} catch (WebDriverException e) {
			reportstep(" alert unhandled ", "fail");
		}

	}

	@Override
	public String getAlertText() {
		try {
			String text = driver.switchTo().alert().getText();
			System.out.println(text);
		} catch (WebDriverException e) {
			System.out.println(" no alert handled ");
		}
		return null;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void clickByXpathNoSnap(String locvalue)
	{
		try {
			driver.findElementByXPath(locvalue).click();
			reportstep("clicked sucesssfully", "pass");
		} catch (WebDriverException e) {
			reportstep(" element not found ", "fail");
		}
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			reportstep("browser closed succussfully", "pass");
		} catch (WebDriverException e) {
			reportstep("browser closed unsuccussfully", "fail");
		}
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		try {
			driver.quit();
			reportstep("all browser closed succussfully", "pass");
		} catch (WebDriverException e) {
			reportstep("all browser closed unsuccussfully" , "fail");
		}

	}

}
