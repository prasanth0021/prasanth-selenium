package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnDrag {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://jqueryui.com/draggable/");
		driver.findElementByLinkText("Draggable").click();
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementById("draggable");
		
		Actions dg = new Actions(driver);
		Thread.sleep(3000);
		dg.dragAndDropBy(drag, 100, 100).perform();
	}

}
