package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomeWorkMerge {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://www.leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		//first img
		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[1]").click();
		Set<String> win = driver.getWindowHandles();
		int size = win.size();
		System.out.println(size);
		List<String> secwin = new ArrayList<String>();
		secwin.addAll(win);
		String w = secwin.get(1);
		driver.switchTo().window(w);
		driver.manage().window().maximize();
		//lead id
		List<WebElement> lead = driver.findElementsByXPath("(//input[@class=' x-form-text x-form-field'])[1]");
		lead.get(0).sendKeys("10680");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a").click();
		String w1 = secwin.get(0);
		driver.switchTo().window(w1);

		//second img
		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[2]").click();
		Set<String> win1 = driver.getWindowHandles();
		int size2 = win.size();
		System.out.println(size2);
		List<String> secwin1 = new ArrayList<String>();
		secwin1.addAll(win1);
		String sw = secwin1.get(1);
		driver.switchTo().window(sw);
		driver.manage().window().maximize();
		List<WebElement> lead1 = driver.findElementsByXPath("(//input[@class=' x-form-text x-form-field'])[1]");
		lead1.get(0).sendKeys("10681");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a").click();
		String sw1 = secwin1.get(0);
		driver.switchTo().window(sw1);
		driver.findElementByLinkText("Merge").click();
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//input[@class=' x-form-text x-form-field ']").sendKeys("10010");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
        //screenshot
		File ss = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snap/img.png");
		FileUtils.copyFile(ss, dest);


	}

}
