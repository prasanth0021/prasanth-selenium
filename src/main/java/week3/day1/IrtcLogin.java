package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrtcLogin {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		//driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("prasanth0021");
		driver.findElementById("userRegistrationForm:password").sendKeys("prasanth12345");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("prasanth12345");
		WebElement userque = driver.findElementById("userRegistrationForm:securityQ");
		Select dd1 = new Select(userque);
		dd1.selectByValue("0");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("bhirava");
		WebElement lang = driver.findElementById("userRegistrationForm:prelan");
		Select dd2 = new Select(lang);
		dd2.selectByValue("en");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("prasanth");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("ravi");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement date = driver.findElementById("userRegistrationForm:dobDay");
		Select dd3 = new Select(date);
		dd3.selectByValue("21");
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select dd4 = new Select(month);
		dd4.selectByValue("09");
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dd5 = new Select(year);
		dd5.selectByValue("1996");
		WebElement occu = driver.findElementById("userRegistrationForm:occupation");
		Select dd6 = new Select(occu);
		dd6.selectByVisibleText("Private");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("5485466444");
		driver.findElementById("userRegistrationForm:idno").sendKeys("DFRSD451224522");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select dd7 = new Select(country);
		dd7.selectByValue("94");
		driver.findElementById("userRegistrationForm:email").sendKeys("prasanth.ravi0021@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9677252181");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select dd8 = new Select(nationality);
		dd8.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("no51,2nd cross st guduvancheery");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603202",Keys.TAB);
		Thread.sleep(3000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select dd9 = new Select(city);
		dd9.selectByValue("Kanchipuram");
		Thread.sleep(3000);
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select dd10 = new Select(post);
		dd10.selectByValue("Guduvanchery S.O");
		Thread.sleep(3000);
		driver.findElementById("userRegistrationForm:landline").sendKeys("9677252181");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
		driver.findElementById("userRegistrationForm:newsletter:0").click();
		driver.findElementById("userRegistrationForm:commercialpromo:0").click();
		driver.findElementById("userRegistrationForm:irctcsbicard:0").click();
		
	}

}
