package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)
		driver.get("http://www.leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("testleaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("prasanth");
		driver.findElementById("createLeadForm_lastName").sendKeys("R");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(src);
		dd.selectByVisibleText("Conference");
		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(marketing);
		dd1.selectByVisibleText("Car and Driver");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("prasanth");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("ravi");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("student");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("fresher");
		driver.findElementById("createLeadForm_departmentName").sendKeys("cse");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("200000");
		WebElement annualrevenue = driver.findElementByName("currencyUomId");
		Select dd2 = new Select(annualrevenue);
		dd2.selectByVisibleText("INR - Indian Rupee");
		//dd2.selectByValue("INR");
		WebElement indenumid = driver.findElementById("createLeadForm_industryEnumId");
		Select dd3 = new Select(indenumid);
		dd3.selectByValue("IND_SOFTWARE");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("10000");
		WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd4 = new Select(owner);
		dd4.selectByValue("OWN_PROPRIETOR");
		driver.findElementById("createLeadForm_sicCode").sendKeys("7546");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("high");
		driver.findElementById("createLeadForm_description").sendKeys(" am learning selenium ");
		driver.findElementByName("importantNote").sendKeys("selenium");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("44");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9677252181");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("prasanth");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("prasanth.ravi0021@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://leaftaps.com/opentaps/");
		driver.findElementById("createLeadForm_generalToName").sendKeys("prasanth");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("prasanth ravi");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("no.51,2nd creoos st,");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("sri mahalakshmi nagar,guduvancheery");
		driver.findElementById("createLeadForm_generalCity").sendKeys("chennai");
		WebElement area = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dd5 = new Select(area);
		dd5.selectByValue("CA");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("4");
		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dd6 = new Select(country);
		dd6.selectByVisibleText("India");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("44");
		driver.findElementByClassName("smallSubmit").click();



	}


}
