package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailTable {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC", Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if (selected) {
			driver.findElementById("chkSelectDateOnly").click();
			
		}
		Thread.sleep(5000);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> tr = table.findElements(By.tagName("tr"));
		System.out.println("row size : " +tr.size());
		//int RC=tr.size();
		WebElement firstrow = tr.get(1);
		List<WebElement> td = firstrow.findElements(By.tagName("td"));
		System.out.println("column size : " +td.size());
		
		for (WebElement eachtr : tr) {
			String text = eachtr.findElements(By.tagName("td")).get(1).getText();
			System.out.println(text);
		}
		
		
	}

}
