package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HomeWorkIrctcDropDowncountryEgypt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i=0;
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		WebElement country = driver.findElementById("userRegistrationForm:countries");
	    Select dd1 = new Select(country);
	    List<WebElement> list = dd1.getOptions();
	    for (WebElement each : list) {
			if (each.getText().startsWith("E")) {
				i++;
				if (i==2) {
					System.out.println(each.getText());
					each.click();
					break;
				}
			}
		}
	    driver.close();
	}

}
