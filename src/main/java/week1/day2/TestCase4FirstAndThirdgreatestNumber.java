package week1.day2;

import java.util.Scanner;

public class TestCase4FirstAndThirdgreatestNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n, temp;//scanner class object creation
		Scanner s = new Scanner(System.in);
 
		
		System.out.print("Enter the elements you want : ");
		n = s.nextInt();

		
		int a[] = new int[n];

		 
		System.out.println("Enter all the elements:");
		for (int i = 0; i < n; i++) 
		{
			a[i] = s.nextInt();
		}

		
		for (int i = 0; i < n; i++) 
		{
			for (int j = i + 1; j < n; j++) 
			{
				if (a[i] < a[j]) 
				{
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}

		//print sorted elements 		
		System.out.println("the first greatest num is "+a[0]+ "\n the third greatest num is "+a[2]);
//		for (int i = 0; i < n ; i++) 
//		{
//			System.out.println(a[i]);
//		}
		s.close();

	}

}
