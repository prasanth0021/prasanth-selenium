package week1.day2;

public class StringVariable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name=" Prasanth ";
		System.out.println(name.length());
		//System.out.println(name.toCharArray());
		
		char[] charArray = name.toCharArray();
		for (char c : charArray) {
			System.out.println(c);
		}
		System.out.println(name.toLowerCase());
		System.out.println(name.toUpperCase());
		System.out.println(name.charAt(5));
		System.out.println(name.equals("divya"));
		System.out.println(name.replace("a", "aa"));
		System.out.println(name.startsWith("P"));
		System.out.println(name.substring(3));System.out.println(name.substring(3, 6));
		System.out.println(name.endsWith("h"));
		System.out.println(name.startsWith("p"));
		System.out.println(name.replaceAll("asa","sas"));
		System.out.println(name.split("s"));
		System.out.println(name.trim());
		System.out.println(name.concat("divya"));
		System.out.println(name.contains("s"));
	//	System.out.println(name.all);
		
	}

}
