package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;
import week6.day1.ReadExcel;

public class ProjectMethods extends SeMethods {

	@DataProvider(name = "fetchData")
	public Object[][] getData() throws IOException {
		Object[][] exceldata = ReadExcel.getExcelData(excelFileName);
		return exceldata;
	}


	
@BeforeMethod(groups = "common")
@Parameters({"browser","userName","password","appUrl"})
public void login(String browser, String uname , String pwd ,String url)
{
	startApp(browser , url);
	WebElement username = locateElement("id", "username");
	type(username, uname);
	WebElement password = locateElement("id", "password");
	type(password, pwd);
	WebElement login = locateElement("class", "decorativeSubmit");
	click(login);
	WebElement CRMSFA = locateElement("linktext", "CRM/SFA");
	click(CRMSFA);
	
}


/*@BeforeMethod(groups = "common")
public void login()
{
	startApp("chrome" , "http://leaftaps.com/opentaps");
	WebElement username = locateElement("id", "username");
	type(username, "DemoSalesManager");
	WebElement password = locateElement("id", "password");
	type(password, "crmsfa");
	WebElement login = locateElement("class", "decorativeSubmit");
	click(login);
	WebElement CRMSFA = locateElement("linktext", "CRM/SFA");
	click(CRMSFA);
}*/

	@AfterMethod (groups = "common")
	public void closebrowser()
	{
		closeAllBrowsers();
	}
}
