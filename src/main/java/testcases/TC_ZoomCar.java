package testcases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_ZoomCar extends ProjectMethods{
	@BeforeClass
	public void setdata()
	{
		tcname ="TC_ZoomCar";
		tcdesc = " book a car ";
		author = " prasanth ";
		catogory = "smoke";
	}
@Test
	public void zoomcar()
	{
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement e1 = locateElement("linktext", "Start your wonderful journey");
		click(e1);
		WebElement e2 = locateElement("xpath", "(//div[@class='items'])[1]");
		click(e2);
		WebElement e3 = locateElement("xpath", "//button[text()='Next']");
		click(e3);
		WebElement e4 = locateElement("xpath", "(//div[@class='days']/div)[2]");
		click(e4);
		WebElement e5 = locateElement("xpath", "//button[text()='Next']");
		click(e5);
		WebElement e6 = locateElement("xpath", "//button[text()='Done']");
		click(e6);
		List<WebElement> e7 = locateElements("xpath", "//div[@class='car-listing']");
		getsize(e7);
		List<WebElement> e8 = locateElements("xpath", "//div[@class='price']");
		//String text = getText(e8);
		List<Integer> pricelist = new ArrayList<>();
		for (WebElement eachcost : e8) {
			String cost = eachcost.getText();
			String eachprice = cost.replaceAll("\\D", "");
			int price = Integer.parseInt(eachprice);
			pricelist.add(price);
			
		}
		
		Collections.sort(pricelist);
		int size = pricelist.size();
		Integer max = pricelist.get(size-1);
		//Integer min = pricelist.get(0);
		WebElement e9 = locateElement("xpath", "//div[contains(text(),'"+max+"')]/preceding::h3[1]");
		getText(e9);
		
		
	
		
		
	}
	

}
