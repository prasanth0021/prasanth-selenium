package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC006_DuplicateLead extends ProjectMethods {
	
	@BeforeClass (groups = "common")
	public void setdata()
	{
		tcname ="TC006_DuplicateLead";
		tcdesc = " duplicate lead ";
		author = " prasanth ";
		catogory = "regression";
		excelFileName = "duplicate";
	}
	@Test(dataProvider = "fetchData")
public void duplicatelead(String fname)  {
		
		
		
		//login();
		WebElement link2 = locateElement("linktext","Leads");
		click(link2);
		
		WebElement link3 = locateElement("linktext","Find Leads");
		click(link3);
		
		WebElement link4 = locateElement("xpath","//div[@class='x-form-item x-tab-item']/div[@class='x-form-element']/input[@name='firstName']");
		type(link4,fname);
		
		WebElement link5 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link5);
		
		
		
		WebElement link6 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(link6);
		
					
		WebElement link7 = locateElement("linktext", "Duplicate Lead");
		click(link7);
		verifyPartialTitle("Duplicate Lead");
		
		WebElement link8 = locateElement("xpath", "//input[@class='smallSubmit']");
		click(link8);
		
		
		}
	
	
}
