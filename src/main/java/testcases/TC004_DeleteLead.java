package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC004_DeleteLead extends ProjectMethods {
	@BeforeClass (groups = "common")
	public void setdata()
	{
		tcname ="TC004_DeleteLead";
		tcdesc = " delete lead ";
		author = " prasanth ";
		catogory = "regression";
		excelFileName = "deletelead";
	}
	
	
	@Test (/*dependsOnMethods = "testcases.TC005_EditLead.EditLead"*//* groups = "regression" , dependsOnGroups = "sanity" ,*/ dataProvider = "fetchData")
	public void DeleteLead(String fname1 ,String fname2) throws InterruptedException
	{
		
		//login();
		WebElement link2 = locateElement("linktext","Leads");
		click(link2);
		
		WebElement link3 = locateElement("linktext","Find Leads");
		click(link3);
		
		WebElement link4 = locateElement("xpath","//a[@href='/crmsfa/control/findLeads']");
		click(link4);
		
		
		
		WebElement link6 = locateElement("xpath", "//div[@class='x-form-item x-tab-item']/following::input");
		type(link6, fname1);
		
			
		WebElement link8 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link8);
		
		Thread.sleep(3000);
		
		WebElement link9 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		getText(link9);
				
		click(link9);
		
		WebElement link10 = locateElement("xpath","//a[@class='subMenuButtonDangerous'][text()='Delete']");
		click(link10);
		
		WebElement link11 = locateElement("linktext","Find Leads");
		click(link11);
		
		WebElement link12 = locateElement("xpath","//div[@class='x-form-item x-tab-item']/following::input");
		type(link12,fname2);
		
		WebElement link13 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link13);
		
		WebElement link14 = locateElement("xpath","//div[@class='x-paging-info']");
		getText(link14);
		
		
		
				
	}
	
	
	
	


}
