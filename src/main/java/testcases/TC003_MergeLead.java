package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class TC003_MergeLead extends ProjectMethods{
	@BeforeClass (groups = "common")
	public void setdata()
	{
		tcname ="TC003_MergeLead";
		tcdesc = " merge lead ";
		author = " prasanth ";
		catogory = "regression";
		excelFileName ="mergelead";
	}
	
	
	@Test(dataProvider = "fetchData")
	public void merge(String fname1 , String fname2 ,String findMlead) 
	{
		//login();
		WebElement e1 = locateElement("linktext", "Leads");
		click(e1);
		WebElement e2 = locateElement("linktext", "Merge Leads");
		click(e2);
		WebElement e3 = locateElement("xpath", "(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[1]");
		click(e3);
		switchToWindow(1);
		//switchToLastWindow();
		WebElement e4 = locateElement("xpath", "(//input[@class=' x-form-text x-form-field'])[2]");
		type(e4, fname1);
		WebElement e5 = locateElement("xpath", "//button[text()='Find Leads']");
		click(e5);
		//WebElement e6 = locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		//click(e6);
		clickByXpathNoSnap("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		switchToWindow(0);
		//switchToParentWindow();
		WebElement e7 = locateElement("xpath", "(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[2]");
		click(e7);
		switchToWindow(1);
		//switchToLastWindow();
		WebElement e8 = locateElement("xpath", "(//input[@class=' x-form-text x-form-field'])[2]");
		type(e8, fname2);
		WebElement e9 = locateElement("xpath", "//button[text()='Find Leads']");
		click(e9);
		//WebElement e10 = locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		//click(e10);
		clickByXpathNoSnap("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		switchToWindow(0);
		//switchToParentWindow();
		WebElement e11 = locateElement("linktext", "Merge");
		clickWithNoSnap(e11);
		//Thread.sleep(2000);
		//getAlertText();
		acceptAlert();
		WebElement e12 = locateElement("linktext", "Find Leads");
		click(e12);
		WebElement e13 = locateElement("xpath", "(//div[@class='x-form-item x-tab-item']//input)[1]");
		type(e13, findMlead);
		WebElement e14 = locateElement("xpath", "//button[text()='Find Leads']");
		click(e14);
		
		


	}
	
	


}
