package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class TC002_CreateLead extends ProjectMethods{
	@BeforeClass (groups = "common")
	public void setdata()
	{
		tcname ="TC002_CreateLead";
		tcdesc = " create lead ";
		author = " prasanth ";
		catogory = "smoke";
		excelFileName = "createlead";
	}
	@Test (/*invocationCount = 2 , invocationTimeOut = 30000,*/ /*groups = "smoke" ,*/ dataProvider = "fetchData")
	public void create(String cname, String fname,
			String lname,String email, String ph)
	{
		//login();
		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		WebElement element1 = locateElement("id", "createLeadForm_companyName");
		type(element1, cname);
		WebElement element2 = locateElement("id", "createLeadForm_firstName");
		type(element2, fname);
		WebElement element3 = locateElement("id", "createLeadForm_lastName");
		type(element3, lname);
		WebElement dd1 = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(dd1, "Direct Mail");
		WebElement dd2 = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(dd2, "Automobile");
		type(locateElement("id", "createLeadForm_primaryEmail"), email);
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ph);
		WebElement e1 = locateElement("xpath", "//input[@class='smallSubmit']");
		click(e1);

	}
}
	




