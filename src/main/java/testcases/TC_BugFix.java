package testcases;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
public class TC_BugFix {


//changes
	@Test
	public void main() throws InterruptedException  {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByLinkText("Jackets").click();
		//switch window
		/*		Set<String> win = driver.getWindowHandles();
			System.out.println(win.size());
			List<String> list = new ArrayList<String>();
			list.addAll(win);
			String secwin = list.get(1);
			driver.switchTo().window(secwin);*/


		// Find the count of Jackets
		//driver.findElementByXPath("//div[@class='brand-more']").click();
		WebElement leftCount = driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span");
		String counttext = leftCount.getText();
		String count = counttext.replaceAll("\\D", "");
		int Lcount = Integer.parseInt(count);
		System.out.println(Lcount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		WebElement rightCount = driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span");
		String counttext1 = rightCount.getText();
		String count1 = counttext1.replaceAll("\\D", "");
		int Rcount = Integer.parseInt(count1);
		System.out.println(Rcount);

		// If both count matches, say success
		if(Lcount==Rcount) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<Integer> onlyPrice = new ArrayList<>();

		for (WebElement productPrice : productsPrice) {
			String cost = productPrice.getText().replaceAll("\\D", "");
			int Price = Integer.parseInt(cost);
			onlyPrice.add(Price);
		}

		// Sort them 

		Integer max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);

		/*driver.close();*/

		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//div[@class='brand-more']").click();
		driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		List<Integer> onlyPrice1 = new ArrayList<>();

		for (WebElement brandPrice : allenSollyPrices) {
			String brandcost = brandPrice.getText().replaceAll("\\D", "");
			int onlycost = Integer.parseInt(brandcost);
			onlyPrice1.add(onlycost);
		}

		// Get the minimum Price 
		Integer min = Collections.min(onlyPrice1);

		// Find the lowest priced Allen Solly
		System.out.println(min);


	}

}

