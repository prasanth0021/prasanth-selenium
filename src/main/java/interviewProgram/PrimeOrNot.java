package interviewProgram;

import java.util.Scanner;

import org.testng.annotations.Test;

public class PrimeOrNot {
@Test(priority=1)
public void primeornot()
{
	int a,i;
	boolean flag = false;
	Scanner scan = new Scanner(System.in);
	System.out.println("enter the value of a:");
	 a = scan.nextInt();
	for(i=2;i<=a/2;i++)
	{
	if(a%i ==0)
	{
		flag = true;
		break;
	}
	}
	if(!flag)
	{
		System.out.println("the given " + a +" is prime");
	}
	else
	{
		System.out.println("the given " + a +" is not prime");
	}
scan.close();
}

}

