package testngsample;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AnnnotationSample {
	
	// BS
	// BC
	// BT
	// BM
	// T
	// AM
	// AT
	// AC
	// AS
	
	@BeforeTest
	public void bf() {
		System.out.println("I am Before Test");
	}
	
	@BeforeClass
	public void bc() {
		System.out.println("I am Before Class");
	}
	
	@Test
	public void t() {
		System.out.println("I am TEST");
	}

}
