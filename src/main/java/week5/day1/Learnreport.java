package week5.day1;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Learnreport {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String tcname , tcdesc;
	public static String author , catogory , excelFileName ;
	
	@BeforeSuite (groups = "common")
	public void startReport()
	{
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/zoomcars.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}

	@BeforeMethod (groups = "common")
	public void startTest()
	{
	 test = extent.createTest(tcname , tcdesc);
		test.assignAuthor(author);
		test.assignCategory(catogory);
	}

	public void reportstep(String desc , String status)
	{
if (status.equalsIgnoreCase("pass")) {
	test.pass(desc);
} if(status.equalsIgnoreCase("fail")){
	test.fail(desc);
}
		
	}
	
	@AfterSuite (groups = "common")
	public void stopresult()
	{
	extent.flush();
	}


}
